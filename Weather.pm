package Weather;
  use LWP::Simple;

  sub set_rss_url{
    my $class = shift;
    my $self = {@_};
    bless ($self, $class);
    return $self;
  }

  sub get_weather{  
    my $url = shift; 
    my $content = get $url->{'url'};
    die "Couldn't get $url" unless defined $content;
    return extract_from_content( $content );
  }
  
  sub extract_from_content($){
    my $content = shift; 
    my @lines = split /\n/, $content;
    foreach my $line (@lines) {
      if ($line =~ /Temperature/) {
        $line =~ m/CDATA\[(.*)\<img/;
        $line = $1;
        $line =~ s/\|/\n/g;
        print $line."\n";
        return $line;
      }
    }
  }

1;
