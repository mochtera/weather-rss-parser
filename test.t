use lib 'C:\Users\mateusz.ochtera\Documents\Mateusz\weather';
use Weather;

use Test::More tests => 5;
use Test::Exception;

my $weather = Weather->set_rss_url('test');
my $good_weather = Weather->set_rss_url( url => 'http://rss.wunderground.com/auto/rss_full/global/stations/12375.xml?units=metric');
my $bad_weather = Weather->set_rss_url(url =>'test');

isa_ok( $weather, 'Weather');
can_ok( $weather, 'get_weather');
dies_ok( sub {$bad_weather->get_weather()}, 'expecting to die on bad URL');
cmp_ok( $good_weather->get_weather() ,'=~', /Temperature/, 'should contain "Temperature"');
lives_ok(  sub {$good_weather->get_weather()}, 'expecting to live on good URL');
done_testing();
