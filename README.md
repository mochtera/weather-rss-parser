# Weather RSS parser

Simple weather parser for later use in conky.

Usage:

    perl wgetter.pl

Response example:

    Temperature: 29&deg;C
    Humidity: 33%
    Pressure: 1022hPa (Steady)
    Conditions: Clear
    Wind Direction: Variable
    Wind Speed: 2km/h
